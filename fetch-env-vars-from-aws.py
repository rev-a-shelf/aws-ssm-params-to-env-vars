#!/usr/bin/python3
import boto3
import time
import os
import shlex

# ------------------------------------------------------------------------------

secret_names_filename = "/etc/ras_secret_names"

secret_names_file = open(secret_names_filename, 'r')
secret_names_text = secret_names_file.read()
secret_names_file.close()

secret_names = secret_names_text.split('\n')
secret_names = [
    name.strip() for name in secret_names if name.strip() != ''
]

# ------------------------------------------------------------------------------

def getSecretVal(name):
    return ssm.get_parameter(Name=name, WithDecryption=True)['Parameter']['Value']

# ==============================================================================

env = os.environ.get('RAS_ENV')

# Select SSM parameters based on the 'RAS_ENV' environment variable
prefix_map = {
    "STAGING": "/staging/",
    "PROD": "/prod/",
}
if env in prefix_map:
    prefix = prefix_map[env]
else:
    raise KeyError("\"RAS_ENV\" environment variable not set, or set to incorrect value. Value: \"%s\"" % (env))


env_vars_script_filepath = "/etc/profile.d/ras_env_vars.sh"
last_run_filepath = "/tmp/.ras_env_vars_last_run"

# First check if secrets have been retrieved recently
do_retrieval = True
retrieval_threshold = 30 # seconds
current_time = time.time()

# Always retrieve secrets if script hasn't been created for first time
if not os.path.exists(env_vars_script_filepath):
    do_retrieval = True
else:
    if not os.path.exists(last_run_filepath):
        do_retrieval = True
    else:
        last_run_file = open(last_run_filepath, 'r')
        last_run_text = last_run_file.read()
        last_run_file.close()

        try:
            last_run_time = int(last_run_text)
        except ValueError:
            last_run_time = 0

        time_since_last_run = current_time - last_run_time

        if time_since_last_run > retrieval_threshold:
            do_retrieval = True
        else:
            do_retrieval = False

if do_retrieval:
    print("Getting values from AWS to create as environment variables...")

    # Connect to AWS
    ssm = boto3.client('ssm', region_name='us-east-1')

    # Fetch each secret, saving its value in a dictionary
    secret_vals = {
        name:getSecretVal(prefix + name) for name in secret_names
    }

    # Output a file which can be `source`-ed to set the environment variables
    export_command="export %s=%s"

    env_vars_script_text = ""
    for name, val in secret_vals.items():
        env_vars_script_text += export_command % (name, shlex.quote(val)) + os.linesep

    with open(env_vars_script_filepath, 'w') as f:
        f.write(env_vars_script_text)

    with open(last_run_filepath, 'w') as f:
        f.write(str(int(current_time)))

    print("Finished.")
else:
    print("Skipping retrieval of environment variable values from AWS.")